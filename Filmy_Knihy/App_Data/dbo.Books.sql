﻿CREATE TABLE [dbo].[Books] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [Name]          NVARCHAR (100) COLLATE CZECH_CS_AS NOT NULL,
    [Nazev]         NVARCHAR (100) COLLATE CZECH_CS_AS NOT NULL,
    [Genre]         NVARCHAR (100) COLLATE CZECH_CS_AS NOT NULL,
    [Author]        NVARCHAR (MAX) COLLATE CZECH_CS_AS NOT NULL,
    [publishedYear] INT            NOT NULL,
    [Description]   NVARCHAR (MAX) COLLATE CZECH_CS_AS NULL,
    CONSTRAINT [PK_dbo.Books] PRIMARY KEY CLUSTERED ([Id] ASC)
);

