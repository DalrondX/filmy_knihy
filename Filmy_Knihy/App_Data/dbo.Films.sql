﻿CREATE TABLE [dbo].[Films] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (100) COLLATE CZECH_CS_AS NOT NULL,
    [Nazev]       NVARCHAR (100) COLLATE CZECH_CS_AS NOT NULL,
    [Genre]       NVARCHAR (100) COLLATE CZECH_CS_AS NOT NULL,
    [ReleaseDate] INT            NOT NULL,
    [Description] NVARCHAR (MAX) COLLATE CZECH_CS_AS NULL,
    CONSTRAINT [PK_dbo.Films] PRIMARY KEY CLUSTERED ([Id] ASC)
);

