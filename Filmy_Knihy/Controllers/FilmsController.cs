﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Filmy_Knihy.Models;
using PagedList;
using PagedList.Mvc;

namespace Filmy_Knihy.Controllers
{
    public class FilmsController : Controller
    {
        readonly AppContext db = new AppContext();

        // GET: Films
        public ActionResult Index(string searchBy, string search, int? page)
        {
            if (searchBy == "Genre")
            {
                return View(db.Films.Where(x => x.Genre.ToLower().Contains(search.ToLower().Trim()) ||
                                       search == null)
                                       .ToList().ToPagedList(page ?? 1, 3));
            }
            else if (searchBy == "Released")
            {
                int search1;
                try
                {
                    search1 = Int32.Parse(search.Trim());
                }
                catch (FormatException Fe)
                {
                    search1 = 0;
                }

                return View(db.Films.Where(x => x.ReleaseDate.Equals(search1) ||
                                       false)
                                       .ToList().ToPagedList(page ?? 1, 3));
            }
            else
            {
                return View(db.Films.Where(x => x.Name.ToLower().Contains(search.ToLower().Trim()) ||
                                           x.Nazev.ToLower().Contains(search.ToLower().Trim()) ||
                                           search == null)
                                           .ToList().ToPagedList(page ?? 1, 3));
            }
        }

        // GET: Films/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var film = db.Films.Find(id);
            if (film == null)
            {
                return HttpNotFound();
            }
            return View(film);
        }

        // GET: Films/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Films/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Nazev,Genre,ReleaseDate,Description")] Film film)
        {
            if (ModelState.IsValid)
            {
                db.Films.Add(film);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(film);
        }

        // GET: Films/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var film = db.Films.Find(id);
            if (film == null)
            {
                return HttpNotFound();
            }
            return View(film);
        }

        // POST: Films/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Nazev,Genre,ReleaseDate,Description")] Film film)
        {
            if (ModelState.IsValid)
            {
                db.Entry(film).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(film);
        }

        // GET: Films/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var film = db.Films.Find(id);
            if (film == null)
            {
                return HttpNotFound();
            }
            return View(film);
        }

        // POST: Films/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var film = db.Films.Find(id);
            db.Films.Remove(film);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
