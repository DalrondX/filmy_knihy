﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Filmy_Knihy.Models
{
    public class Book
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("Title")]
        [MaxLength(100)]
        [Required(ErrorMessage = "Title is required")]
        public string Name { get; set; }

        [DisplayName("Czech title")]
        [MaxLength(100)]
        [Required(ErrorMessage = "A czech title is required")]
        public string Nazev { get; set; }

        [Required(ErrorMessage = "A genre is required")]
        [MaxLength(100)]
        public string Genre { get; set; }

        [Required(ErrorMessage = "An author is required")]
        public string Author { get; set; }

        [DisplayName("Published Year")]
        [Required(ErrorMessage = "Published year is required")]
        public int publishedYear { get; set; }
        public string Description { get; set; }
    }
}