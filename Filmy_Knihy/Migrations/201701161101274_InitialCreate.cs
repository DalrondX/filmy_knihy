namespace Filmy_Knihy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Books",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false),
                    Nazev = c.String(nullable: false),
                    Genre = c.String(nullable: false),
                    Author = c.String(nullable: false),
                    publishedYear = c.Int(nullable: false),
                    Description = c.String(),
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Films",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Nazev = c.String(nullable: false),
                        Genre = c.String(nullable: false),
                        ReleaseDate = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Films");
            DropTable("dbo.Books");
        }
    }
}
