namespace Filmy_Knihy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataAnnotations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Books", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Books", "Nazev", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Books", "Genre", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Films", "Name", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Films", "Nazev", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Films", "Genre", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Films", "Genre", c => c.String(nullable: false));
            AlterColumn("dbo.Films", "Nazev", c => c.String(nullable: false));
            AlterColumn("dbo.Films", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Books", "Genre", c => c.String(nullable: false));
            AlterColumn("dbo.Books", "Nazev", c => c.String(nullable: false));
            AlterColumn("dbo.Books", "Name", c => c.String(nullable: false));
        }
    }
}
