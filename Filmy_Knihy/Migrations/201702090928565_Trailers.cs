namespace Filmy_Knihy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Trailers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Films", "Trailer", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Films", "Trailer");
        }
    }
}
